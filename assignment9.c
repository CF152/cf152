#include<stdio.h>
#include<stdlib.h>
struct book
{
   char Title[20],Author[20];
   int Pages;
   float Price;
};
int main()
{
   struct book B1,B2;
   printf("Enter the details of Book 1: (in the order of title, authour, pages and price)\n");
   scanf("%s %s %d %f",&B1.Title,&B1.Author,&B1.Pages,&B1.Price);
   printf("\nEnter the details of Book 2:\n");
   scanf("%s %s %d %f",&B2.Title,&B2.Author,&B2.Pages,&B2.Price);
   system("cls");
   if(B1.Price>B2.Price)
     printf("\n'%s'is more expensive",B1.Title);
   else if(B2.Price>B1.Price)
     printf("\n'%s' is more expensive",B2.Title);
   else
     printf("\nBoth the books cost the same");
   if(B1.Pages<B2.Pages)
     printf("\n%s has written the book with lesser pages",B1.Author);
   else if(B2.Pages<B1.Pages)
     printf("\n%s has written the book with lesser pages",B2.Author);
   else
     printf("\nBoth the books have equal number of pages");
   return 0;
}