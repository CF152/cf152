#include<stdio.h>
int input();
int reverse(int);
void check(int,int);
void print(int a);
int main()
{
int rev,n,i;
printf("ENTER A NUMBER:\n");
n=input();
rev=reverse(n);
check(n,rev);
print(rev);
return 0;
}
int input()
{
int a;
scanf("%d",&a);
return a;
}
int reverse(int n)
{
int d,s=0;
while(n>0)
{
d=n%10;
s=s*10+d;
n=n/10;
}
return s;
}
void check(int n,int rev)
{
if(rev==n)
printf("PALINDROME NUMBER\n");
else 
printf("NOT A PALINDROME NUMBER\n");
}
void print (int a)
{
printf("THE REVERSED NUMBER IS:%d\n",a);
}
